﻿using System;

/// <summary>
/// Code for the master page
/// </summary>
/// <author>
/// CS4985
/// </author>
/// <summary>
/// Spring 2015
/// </summary>
public partial class Site : System.Web.UI.MasterPage
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
