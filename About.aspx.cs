﻿using System;

/// <summary>
/// Code fiel for the About webpage
/// </summary>
/// <author>
/// CS4985
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class About : System.Web.UI.Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}